package net.techu.data;

import org.springframework.data.mongodb.core.mapping.Document;

//import org.springframework.data.*;
@Document("productosdavido")
public class ProductoMongo {

    //@id
    public String id;
    public String nombre;
    public double precio;

    public ProductoMongo() {}

    public ProductoMongo(String nombre, double precio) {
        this.nombre = nombre;
        this.precio = precio;
    }

    @Override
    public String toString(){
        return String.format("Producto [id=%s, nombre=%s, precio=%s]", id, nombre, precio);
    }







}
